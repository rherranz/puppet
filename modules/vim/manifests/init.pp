# /etc/puppet/modules/vim/manifests/init.pp
class vim {
	package { 'vim':
		ensure => present,
		before => File['/etc/vim/vimrc'],
	}
	file { '/etc/vim/vimrc':
                ensure => present,
                mode => 644,
                require => Package['vim'],
		before => File['/root/.vimrc'],
        }
        file { '/root/.vimrc':
                ensure => present,
                mode => 644,
                require => Package['vim'],
		content => 'syntax on',
        }
}
