#/etc/puppet
class composer {
	exec { "/usr/bin/curl -s https://getcomposer.org/installer | /usr/bin/php -- --install-dir=/usr/local/bin/":
		alias => "composer_install",
		creates => "/usr/local/bin/composer.phar",
		require => Package ["php5"],
	}
	exec { "/bin/cp /usr/local/bin/composer.phar /usr/local/bin/composer":
		alias => "composer_move",
		creates => "/usr/local/bin/composer",
		require => Exec ["composer_install"],
	}
}
