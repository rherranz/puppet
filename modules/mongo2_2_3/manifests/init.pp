# /etc/puppet/modules/php.4/manifests/init.pp
class mongo2_2_3 {
	$packs = ['mongodb-10gen=2.2.3']
	exec { "/usr/bin/apt-key adv --keyserver keyserver.ubuntu.com --recv 7F0CEB10 && echo 'deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen' > /etc/apt/sources.list.d/10gen.list && /usr/bin/apt-get update":
		alias	=> "mongo2_2_3",
		creates	=> "/etc/apt/sources.list.d/10gen.list"
	}
	package { $packs:
		ensure => present,
		alias => "mongo_2_2_3",
		require => Exec["mongo2_2_3"],
		subscribe => File['/etc/mongodb.conf']
	}
	exec {"/bin/echo 'mongodb-10gen hold' | /usr/bin/dpkg --set-selections":
		alias   => "hold_mongo_version",
		require => Package['mongo_2_2_3'],
                unless => "/usr/bin/aptitude search mongodb-10gen | /bin/grep 'i '",
	}
	file {'/etc/mongodb.conf':
                path => '/etc/mongodb.conf',
                ensure => present,
                source => 'puppet:///modules/mongo2_2_3/mongodb.conf',
#                require => Package ['mongodb-10gen'],
        }
}
