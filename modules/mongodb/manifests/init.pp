# /etc/puppet/modules/mongodb/manifests/init.pp
class mongodb {
	package { 'mongodb':
		ensure => present,
		before => File['/etc/mongodb.conf'],
	}
	file { '/etc/mongodb.conf':
		ensure => present,
		mode => 644,
		require => Package['mongodb'],
	}
	service { 'mongodb':
		ensure => running,
		enable => true,
		hasrestart => true,
		hasstatus => true,
		subscribe => File['/etc/mongodb.conf'],
		require => Package['mongodb'],
	}
	exec { "pecl-mongo":
		command => "/usr/bin/pecl install mongo",
		require => Package['php-pear', 'build-essential'],
		unless => "/usr/bin/pecl list | /bin/grep mongo"
		#require => Package['build-essentialr'],
		#subscribe => File['/etc/mongodb.conf'],
	}
}
