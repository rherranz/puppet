#/etc/puppet/modules/php54
define php54::phpini (
	$path = "/etc/php5/fpm/php.ini",
	$aug_prefix = "/files",
) {
	augeas { $path : 
		#notify  => Service[nginx],
		require => Package[php5],
		context => "${aug_prefix}${path}",
		#"/files/etc/php5/fpm/php.ini",
		changes => [
			"set PHP/post_max_size 8M",
			"set PHP/upload_max_filesize 25M",
			'set PHP/memory_limit 256M' 
		],
	}
}
