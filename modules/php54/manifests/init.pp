# /etc/puppet/modules/php.4/manifests/init.pp
class php54 {
	include python-software-properties
	$packs = [php5, php5-dev, php5-fpm, php5-imagick, libcurl4-gnutls-dev, php5-curl, php5-cli, php-apc, php5-gd, libxml2, libxml2-dev, curl]
	exec { "/usr/bin/add-apt-repository ppa:ondrej/php5 && /usr/bin/apt-get update":
		alias	=> "php54_repository",
		require	=> Package["python-software-properties"],
		creates	=> "/etc/apt/sources.list.d/ondrej-php5-precise.list"
	}
	package { $packs:
		ensure => present,
#		alias => "php_packages",
		require => Exec["php54_repository"],
	}
}
