#/etc/puppet
class snmpd {
	package { "snmpd":
		ensure => installed,
	}
	service { "snmpd":
		ensure => running,
	}
#	file {'snmpd.conf':
#                path => '/etc/snmp/snmpd.conf',
#                ensure => present,
#                mode => 0644,
#                source => 'puppet:///modules/snmpd/snmpd.conf',
#                require => Package['snmpd'],
#        }
}
