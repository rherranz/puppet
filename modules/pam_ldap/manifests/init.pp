# /etc/puppet/modules/redis/manifests/init.pp
class pam_ldap {
	package { "libpam-ldapd":
                ensure => present,
	}	
	file { '/etc/nslcd.conf':
		ensure => file,
		source => 'puppet:///modules/pam_ldap/nslcd.conf',
	}
	file { '/etc/nsswitch.conf':
		ensure => file,
		source => 'puppet:///modules/pam_ldap/nsswitch.conf',
	}
	service { 'nslcd':
		ensure => running,
		enable     => true,
		hasrestart => true,
		hasstatus  => true,
		subscribe  => File['/etc/nslcd.conf'],
	}
	exec { 'mkhomedir':
		require => Package["libpam-ldapd"],
		command => "/bin/echo 'session    required   pam_mkhomedir.so skel=/etc/skel/ umask=0022' >> /etc/pam.d/common-account",
		unless => "/bin/cat /etc/pam.d/common-account | /bin/grep homedir",
	}
}
