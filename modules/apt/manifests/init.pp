#/etc/puppet/modules/apt/manifests/init.pp
class apt {

	package { apt: ensure => latest }

	file {'sources.list':
        	path => '/etc/apt/sources.list',
        	ensure => present,
        	mode => 0644,
		source => 'puppet:///modules/apt/sources.list',
		require => Package['apt'],
	}
	exec {'update':
		command => "/usr/bin/aptitude update",
		refreshonly => true,
		subscribe => File['/etc/apt/sources.list'],
	}
}
