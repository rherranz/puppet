#/etc/puppet
class supervisor {
	package { "supervisor":
		ensure => present,
	}
	service { 'supervisor':
                ensure => running,
		require => Package['supervisor'],
		hasrestart => true,
		subscribe => File['/etc/supervisor/conf.d/woppjobs.conf'],
	}
	file {'/etc/supervisor/conf.d/woppjobs.conf':
                path => '/etc/supervisor/conf.d/woppjobs.conf',
                ensure => present,
                source => 'puppet:///modules/supervisor/woppjobs.conf',
                require => Package ['supervisor'],
        }
}
