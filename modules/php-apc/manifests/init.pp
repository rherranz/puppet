# /etc/puppet/modules/ant/manifests/init.pp
class php-apc {
	service { "php5-fpm":
		ensure  => "running",
		enable  => "true",
		require => Package["php-apc"],
	}

	file { "/etc/php5/mods-available/apc.ini":
		owner => root,
		group => root,
		mode => 655,
		source => 'puppet:///modules/php-apc/20-apc.ini',
	}

	file { "/etc/php5/conf.d/20-apc.ini":
		notify => Service["php5-fpm"],
		ensure => 'link',
		target => '/etc/php5/mods-available/apc.ini',
	}
}
