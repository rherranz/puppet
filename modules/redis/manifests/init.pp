# /etc/puppet/modules/redis/manifests/init.pp
class redis {
        include python-software-properties	
	$packs = [php5, php5-dev, php5-fpm, php5-imagick, libcurl4-gnutls-dev, php5-curl, libxml2, libxml2-dev]
	exec { "/usr/bin/add-apt-repository ppa:rwky/redis && /usr/bin/apt-get update":
		alias	=> "redis_repository",
		require	=> Package["python-software-properties"],
		creates	=> "/etc/apt/sources.list.d/rwky-redis-precise.list"
	}
	package { "redis-server":
		ensure => present,
		require => Exec["redis_repository"],
	}
}
