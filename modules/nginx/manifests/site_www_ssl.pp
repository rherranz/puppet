#/etc/puppet
class nginx::site_www_ssl {
	# Ñapa para salir al paso con los sites
        file {'sites-available/www_ssl':
                path => '/etc/nginx/sites-available/www_ssl',
                ensure => present,
                source => 'puppet:///modules/nginx/www_ssl',
                require => Package ['nginx'],
        }
	file { '/etc/nginx/sites-enabled/www_ssl':
        	ensure => 'link',
		target => '/etc/nginx/sites-available/www_ssl',
		require => [ File ['sites-available/www_ssl'], File ['/etc/ssl/wopp/wopp.crt'], File ['/etc/ssl/wopp/privkey.pem']]
	}
	file { '/etc/ssl/wopp':
		ensure => 'directory'
	}
	file {'/etc/ssl/wopp/wopp.crt':
                path => '/etc/ssl/wopp/wopp.crt',
                ensure => present,
                source => 'puppet:///modules/nginx/wopp.crt',
                require => File ['/etc/ssl/wopp'],
        }
	file {'/etc/ssl/wopp/privkey.pem':
                path => '/etc/ssl/wopp/privkey.pem',
                ensure => present,
                source => 'puppet:///modules/nginx/privkey.pem',
                require => File ['/etc/ssl/wopp'],
        }
}
