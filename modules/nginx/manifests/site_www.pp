#/etc/puppet
class nginx::site_www {
	# Ñapa para salir al paso con los sites
        file {'sites-available/www':
                path => '/etc/nginx/sites-available/www',
                ensure => present,
                source => 'puppet:///modules/nginx/www',
                require => Package ['nginx'],
        }
	file { '/etc/nginx/sites-enabled/www':
        	ensure => 'link',
		target => '/etc/nginx/sites-available/www',
		require => File ['sites-available/www'],
	}
}
