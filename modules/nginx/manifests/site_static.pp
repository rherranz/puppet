#/etc/puppet/modules/nginx
class nginx::site_static {
	file {'/etc/nginx/sites-available/static':
		path => '/etc/nginx/sites-available/static',
		ensure => present,
		source => 'puppet:///modules/nginx/static',
		require => Package ['nginx'],
	}
	file { '/etc/nginx/sites-enabled/static':
		ensure => 'link',
		target => '/etc/nginx/sites-available/static',
		require => File ['/etc/nginx/sites-available/static'],
	}
}
