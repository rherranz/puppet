#/etc/puppet
class nginx::site_beta {
	# Ñapa para salir al paso con los sites
        file {'sites-available/beta':
                path => '/etc/nginx/sites-available/beta',
                ensure => present,
                source => 'puppet:///modules/nginx/beta',
                require => Package ['nginx'],
        }
	file { '/etc/nginx/sites-enabled/beta':
        	ensure => 'link',
		target => '/etc/nginx/sites-available/beta',
		require => File ['sites-available/beta'],
	}
}
