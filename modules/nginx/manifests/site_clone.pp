#/etc/puppet
class nginx::site_clone {
	# Ñapa para salir al paso con los sites
        file {'sites-available/clone':
                path => '/etc/nginx/sites-available/clone',
                ensure => present,
                source => 'puppet:///modules/nginx/clone',
                require => Package ['nginx'],
        }
	file { '/etc/nginx/sites-enabled/clone':
        	ensure => 'link',
		target => '/etc/nginx/sites-available/clone',
		require => File ['sites-available/clone'],
	}
}
