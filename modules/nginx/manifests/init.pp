# /etc/puppet/modules/nginx/manifests/init.pp
class nginx {
	package { 'nginx':
		ensure => present,
		before => File['/etc/nginx/nginx.conf'],
	}
	file { '/etc/nginx/nginx.conf':
		ensure => present,
		mode => 655,
		require => Package['nginx'],
	}
	service { 'nginx':
		ensure => running,
		enable => true,
		hasrestart => true,
		hasstatus => true,
		subscribe => [ File['/etc/nginx/nginx.conf'], Augeas['/etc/php5/fpm/php.ini']],
		require => Package['nginx'],
	}
	file { '/etc/nginx/mime.types':
		ensure => present,
		mode => 655,
		source => 'puppet:///modules/nginx/mime.types',
		notify => Service ["php5-fpm"],
	}	
}
