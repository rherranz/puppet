#/etc/puppet/modules/nginx
class nginx::site_webdav {
	file {'sites-available/webdav':
                path => '/etc/nginx/sites-available/webdav',
                ensure => present,
                source => 'puppet:///modules/nginx/webdav',
                require => Package ['nginx'],
        }
	file { '/etc/nginx/sites-enabled/webdav':
               	ensure => 'link',
               	target => '/etc/nginx/sites-available/webdav',
		require => File ['sites-available/webdav'],
	}
	$static_dirs = [ "/srv", "/srv/prod", "/srv/prod/static", "/srv/prod/static/images", "/srv/prod/static/video"]

	file { $static_dirs:
		ensure => "directory",
		owner  => "www-data",
		group  => "www-data",
		mode   => 775,
		require => File["/etc/nginx/sites-enabled/webdav"]
	}
}
