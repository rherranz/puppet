#/etc/puppet
class nginx::site_www_prod {
	# Ñapa para salir al paso con los sites
        file {'sites-available/www_prod':
                path => '/etc/nginx/sites-available/www_prod',
                ensure => present,
                source => 'puppet:///modules/nginx/www_prod',
                require => Package ['nginx'],
        }
	file { '/etc/nginx/sites-enabled/www_prod':
        	ensure => 'link',
		target => '/etc/nginx/sites-available/www_prod',
		require => File ['sites-available/www_prod'],
	}
	$dirs = [ "/srv", "/srv/prod", "/srv/prod/web"]
	file { $dirs:
                ensure => 'directory',
		owner  => "deploy",
		group  => "www-data",
		mode   => 775,
#		recurse => true,
        }
}
