# /etc/puppet/modules/pear/manifests/init.pp
class pear {
	package { 'php-pear':
		ensure => present,
		before => Package['php5'],
	}
}
