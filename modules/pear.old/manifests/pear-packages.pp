# /etc/puppet/modules/pear/manifests/pear-packages.pp
class pear::pear-packages {
	package { 'phpunit':
		ensure => present,
		before => Package['php-pear'],
		provider => pear,
	}
}
