#/etc/puppet/modules/ruby_gems
define ruby_gems::install (
	$package = undef,
) {
	package { $package :
                ensure => installed,
		provider => "gem",
		require => Package ['rubygems'],
	}
}
