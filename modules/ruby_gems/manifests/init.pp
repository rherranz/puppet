# /etc/puppet/modules/ruby_gems
class ruby_gems {
	package { 'rubygems':
		ensure => installed,
	}
}
