# /etc/puppet/modules/ant/manifests/init.pp
class nagios-remote-host {
	
	package { 'nagios-nrpe-server':
		ensure => present,
		before => File['/etc/nagios/nrpe.cfg']
	}
	service { 'nagios-nrpe-server':
		enable => true,
		ensure => running,
		subscribe => File['/etc/nagios/nrpe.cfg']
	}
	file {'/etc/nagios/nrpe.cfg':
                path => '/etc/nagios/nrpe.cfg',
                ensure => present,
                source => 'puppet:///modules/nagios-remote-host/nrpe.cfg',
	}
	file {'/usr/lib/nagios/plugins/check_mem':
                path => '/usr/lib/nagios/plugins/check_mem',
                ensure => present,
		mode => 755,
                source => 'puppet:///modules/nagios-remote-host/check_mem',
	}
	file {'/usr/lib/nagios/plugins/check_swap':
                path => '/usr/lib/nagios/plugins/check_swap',
                ensure => present,
		mode => 755,
                source => 'puppet:///modules/nagios-remote-host/check_swap',
	}
	package { 'bc':
		ensure => present,
	}
}
