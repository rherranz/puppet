# /etc/puppet/modules/python-software-propertiesmanifests/init.pp
#
# NOTA: utilizar unicamente en otros modulos que requieren de este paquete para realizar instalaciones
#
class python-software-properties {
	package { "python-software-properties":
                ensure => present,
        }
}
