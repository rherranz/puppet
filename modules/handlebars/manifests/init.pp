class handlebars {
	package {'handlebars':
		ensure => '1.0.10',
		provider => 'npm', 
	}
}
