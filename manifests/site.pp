#!/etc/puppet/manifests/site.pp
node default {
	include vim
	include snmpd
	include nagios-remote-host
	include pam_ldap
}

# Frontales
node 'fe11', 'fe21' inherits default {
	include php54
	php54::phpini { 'fpm': path => "/etc/php5/fpm/php.ini" }
        php54::phpini { 'cli': path => "/etc/php5/cli/php.ini" }
	include nginx
#	include nginx::site_www
	include nginx::site_www_prod
	include nginx::site_www_ssl
	include composer
	include pear
	include ruby_gems
	ruby_gems::install { 'sass': package => "sass",	}
	ruby_gems::install { 'compass': package => "compass", }
	include supervisor
	include ant
	include php-apc
	include nodejs
        include handlebars
}

# Redis
node 'redis11', 'redis21' inherits default {
	include redis
}

# Mongo
node 'mongo11.wopp.local', 'mongo21.wopp.local' inherits default {
	include compilers
	include php54
	include pear
	include mongo2_2_3
}

# Statics
node 'static11.wopp.dev', 'static21.wopp.dev' inherits default {
	include php54
	php54::phpini { 'fpm': path => "/etc/php5/fpm/php.ini" }
	include nginx
	include nginx::site_static
	include nginx::site_webdav
}

node 'devel.wopp.dev' inherits default {
	include php54
        php54::phpini { 'fpm': path => "/etc/php5/fpm/php.ini" }
        php54::phpini { 'cli': path => "/etc/php5/cli/php.ini" }
        include nginx
        include composer
        include pear
        include ruby_gems
        ruby_gems::install { 'sass': package => "sass", }
        ruby_gems::install { 'compass': package => "compass", }
        include supervisor
        include ant
        include php-apc
        include nodejs
	include nginx::site_beta
	include handlebars
#	pear::package {'PHP_Depend': repository => 'pear.pdepend.org', }
#	pear::package {'PHP_PMD': repository => 'pear.phpmd.org', }
#	pear::package {'phpcpd': repository => 'pear.phpunit.de', }
#	pear::package {'phploc': repository => 'pear.phpunit.de', }
#	pear::package {'PHPDocumentor': }
#	pear::package {'PHP_CodeSniffer': }
#	pear::package {'PHP_CodeBrowser': repository => 'pear.phpunit.de', }
#	pear::package {'phpunit': repository => 'pear.phpunit.de', }
}
